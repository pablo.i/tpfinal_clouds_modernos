const { mk_error_response, mk_ok_response } = require('./utils');
const GooglePlaces = require('node-googleplaces');
const config = require('./config');
const placesApi = new GooglePlaces(config.auth.googleplaces);

function mk_places_response(city, callback) {
  const city_obj = JSON.parse(city);
  const query = {
    place_id: city_obj.place_id
  };
  placesApi.details(query).then(
    (response) => {
      return response.body.result.geometry.location;
    }
  ).then(
    (response) => {
      const params = {
        location: response.lat + ',' + response.lng,
        radius: 2000,
        //rankby : 'prominence',
        types: ['bar']
      };
      return placesApi.nearbySearch(params);
    }
  ).then(
    (response) => {
      callback(response.body.results);
    }
  );
}

function filterResponse(resp) {
  const result = [];
  for (const i in resp) {
    const ret_obj = {
      name: resp[i].name,
      rating: resp[i].rating,
      description: resp[i].vicinity
    };
    result.push(ret_obj);
  }
  return result;
}

function places(req, res) {
  try {
    mk_places_response(req.query.city, (resp) => { res.json(mk_ok_response(filterResponse(resp))); });
  } catch (error) {
    res.json(mk_error_response(error));
  }
}

module.exports = places;