const { mk_error_response, mk_ok_response } = require('./utils.js');
const rp = require('request-promise');

function mk_weather_response(cityName, callback) {

  const yql = `select * from weather.forecast where woeid in (select woeid from geo.places(1) where
  text="`+ cityName + `") and u='c'`;
  const host = 'https://query.yahooapis.com/v1/public/yql';

  const req_params = {
    q: yql
  };
  const params = {
    uri: host,
    qs: req_params,
    json: true,
    encoding: 'utf8'
  };
  rp(params).then((yahoo_response) => {
    const ret_object = {
      lastBuildDate: yahoo_response.query.results.channel.lastBuildDate,
      location: yahoo_response.query.results.channel.location,
      units: yahoo_response.query.results.channel.units,
      atmosphere: yahoo_response.query.results.channel.atmosphere,
      current: yahoo_response.query.results.channel.item.condition,
      forecast: yahoo_response.query.results.channel.item.forecast
    };
    callback(ret_object);
  });
}

module.exports = function (req, res) {
  try {
    const cityName = JSON.parse(req.query.city).name;
    mk_weather_response(cityName, (resp) => { res.json(mk_ok_response(resp)); });

  } catch (error) {
    res.json(mk_error_response(error));
  }
};