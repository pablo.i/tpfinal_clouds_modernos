const Twitter = require('twitter');
const config = require('./config');
const Promise = require('promise');
const { mk_error_response, mk_ok_response } = require('./utils');
/*
library doc - https://www.npmjs.com/package/twitter
twitter API doc - https://developer.twitter.com/en/docs/api-reference-index
twitter API - tweet object - https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object
twitter API - user object - https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/user-object


Obtener user credentials:
  1. Loggearse a twitter
  2. IR a https://apps.twitter.com/
  3. Crear una app y conseguir las credenciales
  4. Poner las credenciales en el archivo config.js
*/

const client = new Twitter(config.auth.twitter);
const yandex = require('yandex-translate')(config.auth.yandex);
const yandexTranslatePromise = require('util').promisify(yandex.translate);

function tweets(req, res) {
  const city = JSON.parse(req.query.city);
  const cityName = city.name;
  const tweet1 = {
    text: '',
    author: ''
  };
  const tweet2 = {
    text: '',
    author: ''
  };
  const params = {
    q: cityName,
    count: 10,
  };
  client.get('search/tweets', params).then(
    (response) => {
      return response.statuses;
    }).then(
    (tweets) => {
      return getMaxFollowedUserFromTweets(tweets);
    }).then(
    (user) => {
      return client.get('statuses/user_timeline', { screen_name: user, count: 2 });
    }).then(
    (user_tweets) => {
      tweet1.text = user_tweets[0].text;
      tweet2.text = user_tweets[1].text;
      tweet1.author = user_tweets[0].user.screen_name;
      tweet2.author = user_tweets[1].user.screen_name;
      const promises = [tweet1, tweet2].map(
        (tweet) => yandexTranslatePromise(tweet.text, { to: 'es' })
      );
      Promise.all(promises).then(
        (ret_values) => {
          try {
            res.json(mk_ok_response(formatTweets([tweet1,tweet2], ret_values)));
          } catch (error){
            res.mk_error_response(error);
          }
        }
      );

    });
}

function getMaxFollowedUserFromTweets(tweets) {
  let max_user = null;
  let max_user_followers = -1;
  for (const i in tweets) {
    if (tweets[i].user.followers_count > max_user_followers) {
      max_user = tweets[i].user.screen_name;
      max_user_followers = tweets[i].user.followers_count;
    }
  }
  return max_user;
}

function formatTweets(data, translateData){
  for (const i in data){
    data[i].text = translateData[i].text;
    data[i].lang = translateData[i].lang.slice(0,2);
  }
  return data;
}

module.exports = tweets;